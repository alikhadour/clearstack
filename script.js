$(function () {
  let click_count = 1;
  $("#close-tabs-btn").on("click", function (event) {
    $("#message").html("");
    if (click_count < 2) {
      console.log("less");
      $("#message").append(
        '<span class="d-block alert alert-primary w-100" role="alert">If Sure Click Again!</span>'
      );
      click_count++;
    } else {
      let count = 0;
      chrome.tabs.query({}, function (tabs) {
        $.each(tabs, function (key, tab) {
          //console.log(tab);
          let tab_url = tab.url;
          var hostname = $("<a>").prop("href", tab_url).prop("hostname");
          // console.log(hostname);
          if (hostname == "stackoverflow.com") {
            chrome.tabs.remove(tab.id, function () {});
            ++count;
          }
        });
        click_count = 1;
        console.log("count " + count);
        if (count > 0)
          $("#message").html(
            `<span class='d-block alert alert-success w-100' role='alert'>Done, Total of ${count} tabs is closed</span>`
          );
        else
          $("#message").html(
            `<span class='d-block alert alert-success w-100' role='alert'>No Tabs To Close.</span>`
          );
      });
    }
  });
});
