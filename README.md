# ClearStack

Have you ever found your Chrome tabs crowded with StackOverflow tabs you’re not using anymore?
then this extension is for you
**What is ClearStack?**
*ClearStack* is a Chrome Extention designed for developers who can't get enough of Stackoverflow answers,
simply in one click you can get rid of all open StackOverflow tabs so your pc runs smother.

 
## How To install
**1.** Clone The project files to your pc.

**2.** Go to **chrome://extensions/** and check the box for **Developer mode** in the top right.

![developer-mode-chrome.jpg](https://www.cnet.com/a/img/hub/2017/01/18/b9cd8c02-4a43-4c32-b5b9-65b5fa4e96bf/developer-mode-chrome.jpg)

**3.** From chrome://extensions/ page click the **Load unpacked extension** button and select the project folder for your extension to install it.
